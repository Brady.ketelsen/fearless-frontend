import React, { useEffect, useState } from "react";

function ConferenceForm() {
  const [locations, setLocations] = useState([]);
  const [name, setName] = useState('');
  const [starts, setStarts] = useState('');
  const [ends, setEnds] = useState('');
  const [description, setDescription] = useState('');
  const [maxP, setMaxP] = useState('');
  const [maxA, setMaxA] = useState('');
  const [location, setLocation] = useState('');

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value)
  }

  const handleStartsChange = (event) => {
    const value = event.target.value;
    setStarts(value)
  }

  const handleEndsChange = (event) => {
    const value = event.target.value;
    setEnds(value)
  }

  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value)
  }

  const handleMaxPChange = (event) => {
    const value = event.target.value;
    setMaxP(value)
  }

  const handleMaxAChange = (event) => {
    const value = event.target.value;
    setMaxA(value)
  }

  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {}
    data["name"] = name;
    data["starts"] = starts;
    data["ends"] = ends;
    data["description"] = description;
    data["max_presentations"] = maxP;
    data["max_attendees"] = maxA;
    data["location"] = location;
    console.log(data);

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    console.log('fetched')
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);

      setName('');
      setStarts('');
      setEnds('');
      setDescription('');
      setMaxP('');
      setMaxA('');
      setLocation('');
    }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name}/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartsChange} required type="date" name="starts" id="starts" className="form-control" value={starts}/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndsChange} required type="date" name="ends" id="ends" className="form-control" value={ends}/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="form mb-3">
                <label htmlFor="description">Description</label>
                <textarea onChange={handleDescriptionChange} required type="" rows="3" name="description" id="description" className="form-control" value={description}></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPChange} placeholder="Maximum presentations" required type="number" id="max_presentations" name="max_presentations"
                  className="form-control" value={maxP}/>
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxAChange} placeholder="Maximum attendees" required type="number" id="max_attendees" name="max_attendees"
                  className="form-control" value={maxA}/>
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} required id="location" name="location" className="form-select">
                  <option key={location.id} value={location}>Choose a location</option>
                  {locations.map(location => {
                    return (
                      <option key={location.id} value={location.id}>
                        {location.name}
                      </option>
                      );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  )
}

export default ConferenceForm;
