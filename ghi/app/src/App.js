import Nav from "./Nav";
import AttendeeList from "./AttendeeList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendConferenceForm from "./AttendConferenceForm";
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";
import ReactDOM from "react-dom/client";
import {
  BrowserRouter,
  RouterProvider,
  Route,
  Routes,
} from "react-router-dom";

// const router = createBrowserRouter([
//   {
//     path: "/",
//     element: <div>Hello world!</div>,
//   },
// ]);

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
    <Nav />
    {/* <div className="container"> */}
      <Routes>
        <Route index element={<MainPage />} />
      </Routes>
      <Routes>
        <Route path="locations">
          <Route path="new" element={<LocationForm />}/>
        </Route>
      </Routes>
      <Routes>
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />}/>
        </Route>
      </Routes>
      <Routes>
        <Route path="attendees">
          <Route path="new" element={<AttendConferenceForm />}/>
        </Route>
      </Routes>
      <Routes>
        <Route path="presentations">
          <Route path="new" element={<PresentationForm />}/>
        </Route>
      </Routes>
      <Routes>
          <Route path="attendees" element={<AttendeeList attendees={props.attendees}/>} />
      </Routes>
      {/* <LocationForm /> */}
      {/* <AttendeeList attendees={props.attendees}/> */}
      {/* <ConferenceForm /> */}
      {/* <AttendConferenceForm /> */}
    {/* </div> */}
    </BrowserRouter>
  );
}

export default App;
